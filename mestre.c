#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>

#define VETORES 1000
#define QTD 10000

//Variáveis utilizadas para uso da biblioteca MPI
int size, rank, msg, source, dest, tag;
int posicao = 0;

int compara(const void * a, const void * b) {
  return (*(int*)a - *(int*)b );
}

int RandomInteger(int low, int high){
    int k;
    double d;
    d = (double) rand( ) / ((double) RAND_MAX + 1);
    k = d * (high - low + 1);
    return low + k;
}

void mestre() {
	int tarefa = 0; //Índice da última tarefa que foi enviada
	int feito = 0; //Número de tarefas processadas	
	int numEscravos; //Número de processos
	int aux, i, k; //Variáveis utilizadas para índices

	//Matriz que será gerada para envio dos vetores aos escravos
	//int matriz[VETORES][QTD];
	//Matriz que receberá os vetores ordenados
	//int saco[VETORES][QTD];

	MPI_Status stat;
	MPI_Comm_size(MPI_COMM_WORLD, &numEscravos);
	
	//Destino é o mestre	
	dest = 1; tag = 0;
	
	//Decrementa pois um é o mestre
	numEscravos--;
	
	//vetorAux = malloc(QTD * sizeof(int));
	
	//Vetor que irá receber os vetores ordenados
	int vetorAux[QTD];	
	int **matriz, **saco; 
    //cria matriz dinamico
    matriz = (int **) malloc(VETORES*sizeof(int *));
    saco = (int **) malloc(VETORES*sizeof(int *));
  
	for (i=0; i < VETORES; i++) {
		matriz[i] = (int *) malloc(QTD*sizeof(int));
		saco[i] = (int *) malloc(QTD*sizeof(int));
	}
	//matriz = malloc(VETORES * sizeof(int));
	//for(aux = 0; aux < VETORES; aux++)
    //matriz[aux] = malloc(5 * sizeof(int));
		
	for(aux = 0; aux < VETORES; aux++) {
		for(k = 0; k < QTD; k++) {
			matriz[aux][k] = RandomInteger(1,1300);		
		}
	}
	
	
	for(aux = 0; aux < VETORES; aux++){
		printf("Vetor %d desordenado. \n", aux);		
		for(k = 0; k < QTD; k++){
			printf("Valor: %d\n", matriz[aux][k]);
		}
		printf("___________________________\n");
	}

	for(aux = 0; aux < numEscravos; aux++){
		MPI_Send(matriz[aux], QTD, MPI_INT, aux+1, tag, MPI_COMM_WORLD);
		//printf("Mestre enviou o vetor %d para o escravo: %d\n", aux, aux+1);
		tarefa++; //Conterá o índice pra próxima tarefa a ser enviada
	}
	
	while(tarefa < VETORES) {
		MPI_Recv(&vetorAux[0], QTD, MPI_INT, MPI_ANY_SOURCE, 555, MPI_COMM_WORLD, &stat);
		dest = stat.MPI_SOURCE;		
		feito++;		
		//printf("Estou enviando mais um vetor para o escravo: %d\n", dest);
		MPI_Send(matriz[tarefa], QTD, MPI_INT, dest, tag, MPI_COMM_WORLD);	
		tarefa++;		
	} 
		
	int fim[5] = {-10};
	for (aux = 0; aux < numEscravos; aux++) 
       MPI_Send(&fim, QTD, MPI_INT, aux+1, tag, MPI_COMM_WORLD);

	while(feito < VETORES) {
    	MPI_Recv(&vetorAux[0], QTD, MPI_INT, MPI_ANY_SOURCE, 555, MPI_COMM_WORLD, &stat);
		dest = stat.MPI_SOURCE;
		feito++;
	}

	for(i = 0; i < VETORES; i++) {
		for(aux = 0; aux < QTD; aux++) {
			saco[i][aux] = vetorAux[aux];			
			printf("Valor %d ensacolado pelo vetor: %d\n", vetorAux[aux], i);		
		}
	}
}

void escravo() {
	MPI_Status stat;
	int i, aux;
	
	//vetor = malloc(QTD * sizeof(int));
	
	int vetor[QTD];

	source = 0; tag = 0, i = 0;
	
	aux = vetor[0];
	while(aux != -10) {
     	MPI_Recv(&vetor[0], QTD, MPI_INT, source, tag, MPI_COMM_WORLD, &stat);
		aux = vetor[0];
		if(aux != -10) {
			qsort(vetor, (size_t) QTD, sizeof(int), compara);
			MPI_Send(&vetor[0], QTD, MPI_INT, 0, 555, MPI_COMM_WORLD);
			for(aux = 0; aux < QTD; aux++) 
				printf("Vetor %d. Valor ordenado: %d\n", i, vetor[aux]); 
		}
		i++;	
	}
	MPI_Finalize();
}

int main(int argc, char **argv) {
	double t1, t2;
	MPI_Status stat;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	t1 = MPI_Wtime();
	if(rank == 0) {
		//printf("É o mestre!\n");
		mestre();    
	}
 	else {
		//printf("É o escravo!\n");
		escravo();		
	}
	t2 = MPI_Wtime();
	
	printf("Tempo de demora: %1.2f\n", t2-t1);
    fflush(stdout);
	return 0; 
}

