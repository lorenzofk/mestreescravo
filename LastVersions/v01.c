#include <stdio.h>
#include <mpi.h>

int size, rank, msg, source, dest, tag;
int vetor[3];

int compara(const void * a, const void * b) {
  return (*(int*)a - *(int*)b );
}

void mestre() {
	int tarefa = 0;
	int numEscravos, aux, msg;
	
	MPI_Status stat;
	MPI_Comm_size(MPI_COMM_WORLD, &numEscravos);
	numEscravos--;

	dest = 1; tag = 0;
	vetor[0] =  10;
	vetor[1] =  14;
	vetor[2] =  6;
	
	//Aqui envia
	MPI_Send(vetor, 3, MPI_INT, dest, tag, MPI_COMM_WORLD);
	
	for(aux = 0; aux < 3; aux++) {	
		printf("Mestre enviou %d para %d.\n", vetor[aux], dest);	
	}  	
	
}

void escravo() {
	MPI_Status stat;
	int aux;
	
	source = 0; tag = 0;
	MPI_Recv(&vetor, 3, MPI_INT, source, tag, MPI_COMM_WORLD, &stat);
	
	for(aux = 0; aux < 3; aux++) {
		printf("Recebi o elemento : %d do vetor do mestre\n", vetor[aux]);
	}
	
	//Ordena
	qsort(vetor, (size_t) 3, sizeof(int), compara);

	printf("Vetor ordenado pelo escravo\n");
	for(aux = 0; aux < 3; aux++) {
		printf("i : %d \n", vetor[aux]);
	}
	
}

int main(int argc, char **argv) {
	MPI_Status stat;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if(rank == 0) {
		printf("É o mestre!\n");
		mestre();    
	}
 	else {
		printf("É o escravo!\n");
		escravo();		
	}

	MPI_Finalize(); 

	return 0; 
}

