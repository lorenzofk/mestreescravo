#include <stdio.h>
#include <mpi.h>

int size, rank, msg, source, dest, tag;
int vetor[3], vetor1[3], vetor2[3];

int compara(const void * a, const void * b) {
  return (*(int*)a - *(int*)b );
}

void mestre() {
	int tarefa = 0;
	int numEscravos, aux, msg;
	
	MPI_Status stat;
	MPI_Comm_size(MPI_COMM_WORLD, &numEscravos);
	numEscravos--;

	dest = 1; tag = 0;
	vetor[0] =  10;
	vetor[1] =  14;
	vetor[2] =  6;

	vetor1[0] = 23;
	vetor1[1] = 12;
	vetor1[2] = 4;

	vetor2[0] = 243;
	vetor2[1] = 112;
	vetor2[2] = 14;
	
	//Aqui envia
	for(aux = 0; aux < numEscravos; aux++){
		MPI_Send(vetor, 3, MPI_INT, dest, tag, MPI_COMM_WORLD);
		MPI_Send(vetor1, 3, MPI_INT, dest, tag, MPI_COMM_WORLD);	
		MPI_Send(vetor2, 3, MPI_INT, dest, tag, MPI_COMM_WORLD);
	}
	
	MPI_Recv(&vetor, 3, MPI_INT, MPI_ANY_SOURCE, 999, MPI_COMM_WORLD, &stat);
	MPI_Recv(&vetor1, 3, MPI_INT, MPI_ANY_SOURCE, 999, MPI_COMM_WORLD, &stat);
    MPI_Recv(&vetor2, 3, MPI_INT, MPI_ANY_SOURCE, 999, MPI_COMM_WORLD, &stat);
	
	for(aux = 0; aux < 3; aux++) {	
		printf("Mestre recebeu %d ordenado para %d.\n", vetor1[aux], dest);	
	}  	
	
}

void escravo() {
	MPI_Status stat;
	int i, aux, numVetores;
	MPI_Comm_size(MPI_COMM_WORLD, &numVetores);
	numVetores--;
	
	source = 0; tag = 0;
	
	for(i = 0; i < numVetores; i++) {
		MPI_Recv(&vetor, 3, MPI_INT, source, tag, MPI_COMM_WORLD, &stat);
		MPI_Recv(&vetor1, 3, MPI_INT, source, tag, MPI_COMM_WORLD, &stat);
		MPI_Recv(&vetor2, 3, MPI_INT, source, tag, MPI_COMM_WORLD, &stat);
	}

	//Ordena
	qsort(vetor, (size_t) 3, sizeof(int), compara);
	qsort(vetor1, (size_t) 3, sizeof(int), compara);
	qsort(vetor2, (size_t) 3, sizeof(int), compara);

	MPI_Send(&vetor, 3, MPI_INT, 0, 999, MPI_COMM_WORLD);	
	MPI_Send(&vetor1, 3, MPI_INT, 0, 999, MPI_COMM_WORLD);	
	MPI_Send(&vetor2, 3, MPI_INT, 0, 999, MPI_COMM_WORLD);	

	MPI_Finalize(); 
}

int main(int argc, char **argv) {
	MPI_Status stat;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if(rank == 0) {
		printf("É o mestre!\n");
		mestre();    
	}
 	else {
		printf("É o escravo!\n");
		escravo();		
	}

	return 0; 
}

