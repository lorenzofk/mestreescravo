#include <stdio.h>
#include <mpi.h>

int size, rank, msg, source, dest, tag;
int *vetor;
int **matriz;

int compara(const void * a, const void * b) {
  return (*(int*)a - *(int*)b );
}

void mestre() {
	int tarefa;
	int numEscravos, aux, msg;
	
	MPI_Status stat;
	MPI_Comm_size(MPI_COMM_WORLD, &numEscravos);
	
	//Destino é o mestre	
	dest = 1; tag = 0;
	
	//Decrementa pois um é o mestre
	numEscravos--;
	
	matriz = malloc(numEscravos * sizeof(int));
	for(aux = 0; aux < numEscravos; aux++)
    	matriz[aux]= malloc(2 * sizeof(int));
		
	matriz[0][0] = 5;
	matriz[0][1] = 2;

	matriz[1][0] = 1215;
	matriz[1][1] = 323;
	
	printf("\n");
	for(aux = 0; aux < numEscravos; aux++){
		printf("Vetor %d desordenado. \n", aux);		
		for(tarefa = 0; tarefa < 2; tarefa++){
			printf("Valor: %d\n", matriz[aux][tarefa]);
		}
		printf("___________________________\n");
	}
	for(aux = 0; aux < numEscravos; aux++){
		MPI_Send(matriz[aux], 2, MPI_INT, aux+1, tag, MPI_COMM_WORLD);
		printf("Enviei o vetor %d\n", aux);
	}

	
	//MPI_Recv(&teste, 2, MPI_INT, MPI_ANY_SOURCE, 999, MPI_COMM_WORLD, &stat);
	
	//for(aux = 0; aux < 2; aux++) {	
		//printf("Mestre recebeu %d ordenado para %d.\n", teste[0][0], dest);	
	//}  	
	
}

void escravo() {
	MPI_Status stat;
	int i, aux, numVetores;
	MPI_Comm_size(MPI_COMM_WORLD, &numVetores);

	numVetores--;
	
	vetor = malloc(numVetores * sizeof(int));
	
	source = 0; tag = 0;

	for(i = 0; i < numVetores; i++) {
		MPI_Recv(&vetor[i], 2, MPI_INT, source, tag, MPI_COMM_WORLD, &stat);
		printf("\nVetor %d recebido do processo %d. \n", i, source);	
		qsort(vetor, (size_t) 2, sizeof(int), compara);
		for(aux = 0; aux < 2; aux++) 
			printf("Vetor %d. Valor ordenado: %d\n", i, vetor[aux]);
	}
	
	//MPI_Send(&teste, 2, MPI_INT, 0, 999, MPI_COMM_WORLD);	
	//MPI_Send(&vetor1, 3, MPI_INT, 0, 999, MPI_COMM_WORLD);	
	//MPI_Send(&vetor2, 3, MPI_INT, 0, 999, MPI_COMM_WORLD);	

	MPI_Finalize(); 
}

int main(int argc, char **argv) {
	MPI_Status stat;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if(rank == 0) {
		printf("É o mestre!\n");
		mestre();    
	}
 	else {
		printf("É o escravo!\n");
		escravo();		
	}

	return 0; 
}

