#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>


#define ARRAY_SIZE 20      // trabalho final com o valores 10.000, 100.000, 1.000.000
int vetor[ARRAY_SIZE];
   
void bs(int n, int * vetor) {
    int c=0, d, troca, trocou =1;

    while(c < (n-1) & trocou) {
        trocou = 0;
        for(d = 0 ; d < n - c - 1; d++)
            if(vetor[d] > vetor[d+1]) {
                troca = vetor[d];
                vetor[d] = vetor[d+1];
                vetor[d+1] = troca;
                trocou = 1;
            }
        c++;
   }
   printf("Vetor Ordenado!\n\n");
}

void inicializa(tam_vetor) {
	int i;

	for(i = 0; i < tam_vetor; i++) {
        vetor[i] = tam_vetor-i;
	}
}

int *interleaving(int vetor[], int tam){
	int *vetor_auxiliar;
	int i1, i2, i_aux;

	vetor_auxiliar = (int *)malloc(sizeof(int) * tam);

	i1 = 0;
	i2 = tam / 2;

	for (i_aux = 0; i_aux < tam; i_aux++) {
		if (((vetor[i1] <= vetor[i2]) && (i1 < (tam / 2))) || (i2 == tam))
			vetor_auxiliar[i_aux] = vetor[i1++];
		else
			vetor_auxiliar[i_aux] = vetor[i2++];
	}
	

	return vetor_auxiliar;
}


int main(int argc, char **argv) {
    int tam_vetor, rank;
	int delta = 10;
	int *vetor_aux;
	int pai;
	int tag = 999;

    MPI_Status stat;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);


	//Recebo vetor
	if(rank != 0) {
		MPI_Recv(&vetor, ARRAY_SIZE, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &stat);
		MPI_Get_count(&stat, MPI_INT, &tam_vetor);
		pai = stat.MPI_SOURCE;

		printf("Sou o processo: %d\nO tamanho do vetor que eu recebi é: %d e meu pai é: %d\n", rank, tam_vetor, stat.MPI_SOURCE);  
	}
	else {
   		tam_vetor = ARRAY_SIZE;   	//Defino tamanho inicial do vetor
   		inicializa(tam_vetor);     //Sou a raiz e portanto gero o vetor - ordem reversa
   	}

	//Dividir ou conquistar?
	if(tam_vetor <= delta) {
		printf("Fui ordenado pelo processo: %d\n", rank);   		
		bs(tam_vetor, vetor);
	}
  	else {
		printf("Meu rank: %d. Estou mandando para o processo: %d e %d\n", rank, (rank*2+1), (rank*2+2));
		MPI_Send(&vetor[0], tam_vetor/2, MPI_INT, ((rank*2)+1), tag, MPI_COMM_WORLD);  // mando metade inicial do vetor
   	 	MPI_Send(&vetor[tam_vetor/2], tam_vetor/2, MPI_INT, ((rank*2)+2), tag, MPI_COMM_WORLD);  // mando metade final 
		
		//Receber dos filhos
		MPI_Recv(&vetor[0], tam_vetor/2, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &stat);   
		MPI_Recv(&vetor[tam_vetor/2], tam_vetor/2, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &stat);   
		
		vetor_aux = interleaving(vetor, tam_vetor);	
		
		int teste;
		for(teste = 0; teste < ARRAY_SIZE; teste++) {
			printf("Elemento: %d\n", vetor_aux[teste]);		
		}
	}	

	if(rank != 0) {
		printf("Processo %d enviando para o pai: %d\n", rank, pai);		
		MPI_Send(vetor_aux, ARRAY_SIZE, MPI_INT, pai, tag, MPI_COMM_WORLD);  // mando metade inicial do vetor
	}
	else    
		printf("Devo mostrar\n");
	
	MPI_Finalize();
	return 0;
}
